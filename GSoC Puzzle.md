# Monumento

<img src = "/images/Logo.jpg" height = "300" width="300" align="left" hspace="10" vspace="10"> 

The application will help its users in maintaining statistical information of the users and the number of references to a monument. It will allow the users to click a photograph and based on the picture displays information from Wikipedia. It even allows the textual search of any monument. It notifies the user about more such monuments/landmarks in the vicinity. Also, the app communicates in whatever language the user is comfortable in. The app will also help its users to have an augmented reality view to have a glance before visiting the place. The app can read aloud the details for people who have difficulty in reading or are visually impaired. The details can be generated in any language according to the user’s comfort. Languages like Kotlin and XML and concepts of Android Development and Machine Learning\AR were employed in development. In a nutshell, the application is a one-stop solution to travelers. 

## Flow of App with Mockups

**Splash Screen Activity**

<img src="/images/mockups/SplashScreem.jpg" height="320" width="180">

After the splash screen the app will ask for *Authentication*.

**Login/SignUp Activity**

<img src="/images/mockups/LoginScreen.jpg" height="320" width="180">

After successful signup or login a home screen will appear, which will have certain options which can be operated using *Bottom Navigation Bar*. The primary option will recognize any monument through capturing a image. On the same screen, there will be a Floating Button, which will enable the user to search any monument through text.

<table align="center">
	<tr>
		<td>
			HomeFragment
		</td>
		<td>
			Text Search Activity
		</td>
	</tr>
	<tr>
		<td>
			<img src="/images/mockups/HomeScreen.jpg" height="320" width="180">
		</td>
		<td><img src="/images/mockups/TextSearch.jpg" height="320" width="180">
		</td>
	</tr>
</table>

In Bottom Navigation Bar, left button is for the Profile Fragment, through which user can look into the his/her information and his/her travel logs or travel history.More option is provided on the right which possess various app settings and log out option.

<table align="center">
	<tr>
		<td>
			Profile Fragment
		</td>
		<td>
			More Fragment
		</td>
	</tr>
	<tr>
		<td>
			<img src="/images/mockups/ProfileScreen.jpg" height="320" width="180">
		</td>
		<td><img src="/images/mockups/MoreScreen.jpg" height="320" width="180">
		</td>
	</tr>
</table>


After,the app recognizes any monument through an image or textual information,a screen with the details about the monument through Wikipedia will appear. In this screen a *Nested Scroll View* is implemented for a better UI Design.

While scrolling the details, a button will appear which helps the user to have **Augmented Reality** view of the monument.

Also, this screen or activity will have certain features, which will give a good user experience like **Language Translation**, **Text to Speech**, **Review About the Monument**,**Share on Social Media Platforms**.

<table align="center">
	<tr>
		<td>
			Details Activity
		</td>
		<td>
			Details Activity After Scrolling
		</td>
		<td>
			Augemented Reality View
		</td>
	</tr>
	<tr>
		<td>
			<img src="/images/mockups/DetailScreen.jpg" height="320" width="180">
		</td>
		<td><img src="/images/mockups/DetailScreen after Scrolling.jpg" height="320" width="180">
		</td>
		<td>
			<img src="/images/mockups/ARScreen.jpg" height="320" width="180">
		</td>
	</tr>
</table>

## Flow Diagram

<img src="/images/Monumento Flow Diagram.png">

## Features

The various features in Monumento App.

- **Recognization**: Recognize the monument through capturing an image or through textual search.
- **Augmented Reality**: Augmented Reality view to have a glance before visiting the place.
- **Language Translation**: App communicates in whatever language the user is comfortable in.The details can be generated in any language according to the user’s comfort.
- **Text-to-Speech**: The app can read aloud the details for people who have difficulty in reading or are visually impaired.
- **Reviews About Monument**: The app will allow user to add a review about the monument like the facilities provided there, about the architecture of the monument etc.The review can be submitted immediately or the app will generate a notification after 4 hrs of recognization, to submit a review.
- **Share on Social Media Platform**: The user can share the picture that he/she has clicked on the social media platforms directly from the app. 

## Technology Stack

- Kotlin as Programming Language
- Android Studio as IDE
- XML for Layout Design
- ARCore for generation of AR view
- Firebase for authencation, MLKit



